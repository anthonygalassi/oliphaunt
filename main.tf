resource "aws_instance" "example" {
  count = var.instance_count
  ami           = lookup(var.ami, var.aws_region)
  instance_type = var.instance_type

  // tagging our instance so we can keep track of them
  tags = {
    Name = "Terraform-${count.index + 1}"
    batch = "Second Commit on Project"
  }
}
