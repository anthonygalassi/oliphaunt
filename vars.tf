variable "instance_count" {
  default = "10"
}

variable "instance_type" {
  default = "t3.micro"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "ami" {
  type = map
  default = {
    // we'll be sticking to version 18.04 of ubuntu for now
    "us-west-2" = "ami-058516e5015b8db5f",
    "us-west-1" = "ami-016206eceb305ca0e"
  }
}
